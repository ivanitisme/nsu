import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';

import {Container, Content, Icon, Card, CardItem, Body, Text, Right} from 'native-base';

export default class Lesson extends Component {
    render() {
        const {name, place, teacher_name, time_start, time_end, type} = this.props.lesson;
        return (
            <Card>
                <CardItem header>
                    <Text>{name}</Text>
                </CardItem>

                <CardItem>
                    <Body>
                    <Text>{place}</Text>
                    <Text>{teacher_name}</Text>
                    </Body>
                    <Right>
                        <Text>{time_start} - {time_end}</Text>
                    </Right>
                </CardItem>

            </Card>
        );
    }
}