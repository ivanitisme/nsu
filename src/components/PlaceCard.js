import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Image,
    View
} from 'react-native';

import Icon from 'react-native-vector-icons/Entypo';
import {Container, Content, Card, CardItem, Left, Body, Right, Thumbnail, Text, Button} from 'native-base';

export default class PlaceCard extends Component {

    render() {
        const {id, title, type, descr, time, place, phone, price, img, lat, long, site} = this.props.place;
        const {position} = this.props;

        imageView = img ? <Image style={styles.image}
                                 source={{uri: img}}/> : null;

        return (
            <Card>
                <CardItem>
                    <Left>
                        <Body>
                        <Text>{title}</Text>
                        <Text note>{type}</Text>
                        </Body>
                    </Left>
                </CardItem>

                <CardItem cardBody>
                    {imageView}
                </CardItem>
                <CardItem>
                    <Body>
                    <Text style={{fontSize: 12}}>{descr}</Text>
                    </Body>
                </CardItem>

                <CardItem>
                    <Body>
                    <Text style={{fontSize: 12}}>Средний чек: {price}</Text>
                    </Body>
                </CardItem>

                <CardItem>
                    <Body>
                    <Text style={{fontSize: 12}}>{place}</Text>
                    </Body>
                    <Right>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                            <Icon style={{fontSize: 20}} name="location-pin"/>
                            {this.renderDistance(lat, long, position)}
                        </View>
                    </Right>
                </CardItem>
            </Card>
        );
    }

    getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        const R = 6371;
        const dLat = this.deg2rad(lat2 - lat1);
        const dLon = this.deg2rad(lon2 - lon1);
        const a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c;

    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    renderDistance(lat, long, position) {
        if (position.coords) {
            const
                lat1 = parseFloat(lat),
                long1 = parseFloat(long),
                lat2 = position.coords.latitude,
                long2 = position.coords.longitude;
            const result = this.getDistanceFromLatLonInKm(lat1, long1, lat2, long2);
            return (
                <Text>{(1000 * result).toFixed(0)} м</Text>
            )
        }
        return null;
    }
}


const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: 'cover',
        height: 200,
        width: null,
    }
});