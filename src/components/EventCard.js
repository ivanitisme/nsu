import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';

export default class EventCard extends Component {
    render() {
        const {title, descr, link, img, type, date, section, issue_date} = this.props.event;
        let imgUrl = img || 'https://www.3dnews.ru/_imgdata/img/2009/11/11/149235.jpg';
        return (
            <View style={styles.container}>
                <Image
                    style={[styles.image, this.props.style]}
                    source={{uri: imgUrl}}>
                    <View style={styles.textContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.name}>{date}</Text>
                        </View>

                        <View style={styles.descriptionContainer}>
                            <View style={{padding: 15, backgroundColor: 'rgba(0,0,0,0.4)'}}>
                                <Text style={styles.title}>
                                    {title}
                                </Text>
                                <Text style={styles.description}>
                                    {descr}
                                </Text>
                            </View>
                        </View>
                    </View>
                </Image>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: null,
        height: 300,
        resizeMode: 'cover',
        zIndex: 1
    },
    textContainer: {
        flex: 1,
        backgroundColor: 'transparent',
        zIndex: 2
    },
    name: {
        color: 'white',
        fontSize: 14,
    },
    description: {
        color: 'white',
        fontSize: 14,
    },
    title: {
        color: 'white',
        fontSize: 20,
        fontWeight: '600'
    },
    titleContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 10,
        backgroundColor: 'rgba(0,0,0,0.4)',
    },
    descriptionContainer: {
        flex: 9,
        justifyContent: 'flex-end',
        paddingBottom: 30,
    }
});