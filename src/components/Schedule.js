import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';

import Lesson from './Lesson';

import {Container, Content, Card, CardItem, Body, Text} from 'native-base';

export default class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lessons: null,
            loaded: false
        }
    }

    componentDidMount() {
        const {lessons} = this.props;
        this.sortLessons(lessons);
    }

    sortLessons(lessons) {
        const sortedLessons = lessons.sort(this.compare);
        this.setState({lessons: sortedLessons, loaded: true});

    }

    compare(a, b) {
        return new Date(a.time_start).getTime() - new Date(b.time_start).getTime();
    }

    render() {
        const {loaded, lessons} = this.state;
        if (!loaded) {
            return (
                <View>
                    <Text>Loading</Text>
                </View>
            );
        }

        const lessonsView = lessons.map((lesson) => {
            return (
                <Lesson key={lesson.name} lesson={lesson}/>
            );
        });
        return (
            <Container>
                <Content>
                    {lessonsView}
                </Content>
            </Container>
        );


    }
}