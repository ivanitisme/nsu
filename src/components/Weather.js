import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Api from '../Api';

export default class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            weather: null
        }
    }

    componentDidMount() {
        this.loadWeather();
    }

    async loadWeather() {
        try {
            this.setState({loading: true});
            let response = await Api.getWeather();
            let weather = await response.json();
            weather = weather.weather;
            this.setState({loading: false, weather});
        } catch (error) {
            console.log(error);
        }

    }

    render() {
        const {loading, weather} = this.state;
        if (loading) {
            return this.renderLoading();
        }
        let iconName = 'cloud';
        switch (weather.phrase) {
            case "broken clouds":
                iconName = 'cloud';
        }
        return (
            <View style={[styles.container, this.props.style]}>
                <View style={styles.row}>
                    <Icon style={styles.icon} name={iconName}/>
                    <Text style={styles.text}>{weather.temp} °C</Text>
                </View>

            </View>
        )
    }

    renderLoading() {
        return (
            <View>
                <Text style={styles.text}>Грузим погоду</Text>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        color: 'white',
        margin: 10
    },
    icon: {
        color: 'white',
        fontSize: 25
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
});