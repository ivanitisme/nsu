import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Image,
    View
} from 'react-native';

import {Container, Content, Card, CardItem, Left, Body, Right, Thumbnail, Text, Button, Icon} from 'native-base';

export default class NewsCard extends Component {
    render() {
        let {title, descr, link, img, type, date, section, issue_date} = this.props.news;
        imageView = img ? <Image style={{ resizeMode: 'cover', width: null, flex: 1, height: 100 }}
                                 source={{uri: img}}/> : null;
        return (
            <Card>
                <CardItem>
                    <Left>
                        <Body>
                        <Text>{title}</Text>
                        <Text note>{type}</Text>
                        </Body>
                    </Left>
                </CardItem>

                <CardItem cardBody>
                    {imageView}
                </CardItem>

                <CardItem>
                    <Body>
                    <Text>
                        {descr}
                    </Text>
                    </Body>
                </CardItem>
            </Card>
        );
    }
}