import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Platform,
    Text,
    View
} from 'react-native';

import {Router, Scene} from 'react-native-router-flux';

import SplashScreen from './screens/SplashScreen';
import NewsFeedScreen from './screens/NewsFeedScreen';
import NewsDetailScreen from './screens/NewsDetailScreen';
import PlacesScreen from './screens/PlacesScreen';
import MenuScreen from './screens/MenuScreen';
import ScheduleScreen from './screens/ScheduleScreen';
import SettingsScreen from './screens/SettingsScreen';
import AcademScreen from './screens/AcademScreen';
import GroupSettings from './screens/GroupSettings';

const hideNavBar = Platform.OS === 'android';
export default class App extends Component {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;
    }

    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene key="splash"
                           component={SplashScreen}
                           hideNavBar={true}
                           initial={true}
                    />

                    <Scene key="menu"
                           component={MenuScreen}
                           hideNavBar={true}
                    />

                    <Scene key="schedule"
                           title="Расписание"
                           component={ScheduleScreen}
                           sceneStyle={styles.scene}
                           hideNavBar={false}
                    />

                    <Scene key="academ"
                           title="Академ"
                           component={AcademScreen}
                           hideNavBar={hideNavBar}
                           sceneStyle={styles.scene}
                    />

                    <Scene key="newsFeed"
                           title="Новости"
                           component={NewsFeedScreen}
                           hideNavBar={hideNavBar}
                           sceneStyle={styles.scene}
                    />
                    <Scene key="news"
                           component={NewsDetailScreen}
                           hideNavBar={hideNavBar}
                           sceneStyle={styles.scene}
                    />

                    <Scene key="settings"
                           title="Настройки"
                           component={SettingsScreen}
                           hideNavBar={hideNavBar}
                           sceneStyle={styles.scene}
                    />
                    <Scene key="group"
                           title="Группы"
                           component={GroupSettings}
                           hideNavBar={hideNavBar}
                           sceneStyle={styles.scene}/>
                </Scene>
            </Router>
        );
    }
}
const styles = StyleSheet.create({
    scene: {
        flex: 1,
        paddingTop: 65
    }
});