import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    AsyncStorage,
    View
} from 'react-native';

import {
    Container,
    Right,
    Radio,
    Header,
    Item,
    Icon,
    Input,
    Spinner,
    Content,
    List,
    ListItem,
    Text
} from 'native-base';

import {Actions, ActionConst} from 'react-native-router-flux'
import Api from '../Api';
import Config, {SETTINGS_KEY} from '../Configuration';

export default class GroupSettings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            classes: [],
            filteredClasses: [],
            input: ''
        };
        this.onChangeInput = this.onChangeInput.bind(this);
    }

    componentDidMount() {
        this.loadGroups();
    }

    render() {
        const {loading, input, filteredClasses} = this.state;
        if (loading) {
            return this.renderLoading();
        }
        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name="search"/>
                        <Input
                            value={input}
                            onChangeText={this.onChangeInput}
                            placeholder="Search"/>
                        <Icon active name="people"/>
                    </Item>
                </Header>
                <Content>
                    <List dataArray={filteredClasses} renderRow={(data) =>
                        <ListItem onPress={() => this.onGroupPress(data)}>
                            <Text>{data.name}</Text>
                            <Right>
                                <Radio selected={data.id === Config.groupId} />
                            </Right>
                        </ListItem>
                    }/>
                </Content>
            </Container>
        );
    }

    async onGroupPress(data) {
        Config.groupId = data.id;
        await AsyncStorage.setItem(SETTINGS_KEY, JSON.stringify(Config));
        const {next} = this.props;
        if (next) {
            next({type: ActionConst.REPLACE});
        } else {
            Actions.pop();
        }
    }

    onChangeInput(input) {
        const {classes} = this.state;
        const filteredClasses = classes.filter((item) => input === '' || item.name.indexOf(input) !== -1);
        this.setState({filteredClasses, input});
    }

    async loadGroups() {
        try {
            this.setState({loading: true});
            const response = await Api.getClasses();
            let classes = await response.json();
            classes = classes.classes;
            this.setState({loading: false, classes});
        } catch (error) {
            console.log(error);
        }

    }

    renderLoading() {
        return (
            <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                <Spinner />
                <Text>Loading...</Text>
            </View>
        );
    }

}