import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';

import {Container, Content, ListItem, Text, Icon, Right} from 'native-base';
import {Actions, ActionConst} from 'react-native-router-flux'

export default class SettingsScreen extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container>
                <Content>
                    <ListItem onPress={Actions.group}>
                        <Text>Группа</Text>
                        <Right>
                            <Icon name="arrow-forward" />
                        </Right>
                    </ListItem>
                </Content>
            </Container>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});