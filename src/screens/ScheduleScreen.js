import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

const ScrollableTabView = require('react-native-scrollable-tab-view');
import {Spinner} from 'native-base';
import Schedule from '../components/Schedule';

import Api from '../Api';
import Config from '../Configuration';

export default class ScheduleScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            lessons: null
        }
    }

    componentDidMount() {
        this.loadSchedule();
    }

    async loadSchedule() {
        const data = [
            [], [], [], [], [], [], []
        ];
        try {
            this.setState({loading: true});
            const response = await Api.getLessons(Config.groupId);
            let lessons = await response.json();
            lessons = lessons.lessons;
            lessons.forEach((lesson) => {
                lesson.days.forEach((day) => {
                    const d = new Date(day).getDay();
                    if (data[d].indexOf(lesson) === -1) {
                        data[d].push(lesson);
                    }
                });

            });
            this.setState({loading: false, lessons: data});
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        const {loading, lessons} = this.state;
        if (loading) {
            return this.renderLoading();
        }
        return (
            <ScrollableTabView>
                <Schedule tabLabel="Пн" lessons={lessons[1]}/>
                <Schedule tabLabel="Вт" lessons={lessons[2]}/>
                <Schedule tabLabel="Ср" lessons={lessons[3]}/>
                <Schedule tabLabel="Чт" lessons={lessons[4]}/>
                <Schedule tabLabel="Пт" lessons={lessons[5]}/>
                <Schedule tabLabel="Сб" lessons={lessons[6]}/>
            </ScrollableTabView>
        );
    }

    renderLoading() {
        return (
            <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                <Spinner />
                <Text>Loading...</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});