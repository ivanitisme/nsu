import React, {Component} from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    ToastAndroid,
    View
} from 'react-native';

import {Scene, Router, TabBar, Modal, Actions, Reducer, ActionConst} from 'react-native-router-flux'
import {Spinner} from 'native-base';
import Api from '../Api';
import NewsCard from '../components/NewsCard';

export default class NewsFeedScreen extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            loading: true,
            news: ds.cloneWithRows([])
        };
        this.renderRow = this.renderRow.bind(this);
    }

    componentDidMount() {
        this.loadNews();
    }

    async loadNews() {
        try {
            this.setState({loading: true});
            let response = await Api.getNews();
            let news = await response.json();
            this.setState({loading: false, news: this.state.news.cloneWithRows(news.news)});
        } catch (error) {
            ToastAndroid.show('Hello' + JSON.stringify(error), ToastAndroid.LONG);
            console.log(error);
        }
    };

    render() {
        if (this.state.loading) {
            return this.renderLoading();
        }

        return (
            <View style={styles.container}>
                <ListView
                    dataSource={this.state.news}
                    renderRow={this.renderRow}
                />
            </View>
        );
    }

    renderRow(row) {
        return <NewsCard news={row}/>
    }

    renderLoading() {
        return (
            <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                <Spinner />
                <Text>Loading...</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    }
});