import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    Image,
    View
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Scene, Router, TabBar, Modal, Actions, Reducer, ActionConst} from 'react-native-router-flux'
import Config from '../Configuration';
import Weather from '../components/Weather';


export default class MenuScreen extends Component {

    constructor(props) {
        super(props);
        this.onSchedulePress = this.onSchedulePress.bind(this);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.menu}>
                    <Image
                        style={styles.image}
                        resizeMode="contain"
                        source={require('../../resources/menu_img_weather.png')}>
                        <View style={styles.menuContainer}>
                            <Weather style={{flex:5}}/>
                            <View style={{flex: 1, justifyContent: 'center',alignItems: 'center'}}>
                                <Icon
                                    onPress={Actions.settings}
                                    style={{color: 'white', fontSize: 20}} name="gear"/>
                            </View>
                        </View>
                    </Image>


                </View>

                <TouchableOpacity style={styles.menu} onPress={this.onSchedulePress}>
                    <Image
                        style={styles.image}
                        resizeMode="contain"
                        source={require('../../resources/menu_img_schedule.png')}>
                        <View style={styles.menuContainer}>
                            <Text style={styles.title}>РАСПИСАНИЕ</Text>
                        </View>
                    </Image>

                </TouchableOpacity>

                <TouchableOpacity style={styles.menu} onPress={Actions.newsFeed}>
                    <Image
                        style={styles.image}
                        resizeMode="contain"
                        source={require('../../resources/menu_img_nsu_news.png')}>
                        <View style={styles.menuContainer}>
                            <Text style={styles.title}>НОВОСТИ</Text>
                        </View>
                    </Image>
                </TouchableOpacity>

                <TouchableOpacity style={styles.menu} onPress={Actions.academ}>
                    <Image
                        style={styles.image}
                        resizeMode="contain"
                        source={require('../../resources/menu_img_academ.png')}>
                        <View style={styles.menuContainer}>
                            <Text style={styles.title}>АКАДЕМ</Text>
                        </View>
                    </Image>
                </TouchableOpacity>
            </View>
        );
    }

    onSchedulePress() {
        if (Config.groupId) {
            Actions.schedule();
        } else {
            Actions.group({next: Actions.schedule});
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    menu: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        flex: 1,
        zIndex: 1,
    },
    title: {
        zIndex: 2,
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: 20,
        fontWeight: '600'
    },
    menuContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.3)'
    }
});