import React, {Component} from 'react';
import {
    AppRegistry,
    AsyncStorage,
    StyleSheet,
    Text,
    Image,
    View
} from 'react-native';

import {Actions, ActionConst} from 'react-native-router-flux'
import Config, {SETTINGS_KEY} from '../Configuration';

export default class SplashScreen extends Component {
    constructor(props) {
        super(props);
        setTimeout(() => {

        }, 1000);
    }

    componentDidMount() {
        this.loadConfig();
    }

    async loadConfig() {
        try {
            const configString = await AsyncStorage.getItem(SETTINGS_KEY);
            const config = JSON.parse(configString);
            Object.assign(Config, config);
        } catch (error) {
            console.log(error);
        }
        Actions.menu({type: ActionConst.RESET});
    }

    render() {
        return (
            <Image
                resizeMode="cover"
                source={{uri: 'https://newsib.net/wp-content/uploads/2016/12/5241877abda6b31016b65d3183212730.jpg'}}
                style={styles.container}>
                <Image
                    style={styles.logo}
                    source={require('../../resources/icon_android.png')}/>
            </Image>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: null,
        width: null
    },
    logo: {
        height: 100,
        width: 100
    }
});