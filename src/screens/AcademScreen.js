import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

const ScrollableTabView = require('react-native-scrollable-tab-view');
import EventsScreen from './EventsScreen';
import PlacesScreen from './PlacesScreen';

export default class AcademScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollableTabView>
                    <EventsScreen tabLabel="События"/>
                    <PlacesScreen tabLabel="Места"/>
                </ScrollableTabView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});