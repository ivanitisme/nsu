import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from 'react-native';

import {Scene, Router, TabBar, Modal, Actions, Reducer, ActionConst} from 'react-native-router-flux'
import {Spinner} from 'native-base';

import Api from '../Api';
import EventCard from '../components/EventCard';

export default class EventsScreen extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            loading: true,
            events: ds.cloneWithRows([])
        };
        this.renderRow = this.renderRow.bind(this);
    }

    componentDidMount() {
        this.loadEvents();
    }

    render() {
        if (this.state.loading) {
            return this.renderLoading();
        }

        return (
            <View style={{flex: 1}}>
                <ListView
                    dataSource={this.state.events}
                    renderRow={this.renderRow}
                />
            </View>
        );
    }

    async loadEvents() {
        try {
            this.setState({loading: true});
            let response = await Api.getAcadem();
            let events = await response.json();
            events = events.academ;
            this.setState({loading: false, events: this.state.events.cloneWithRows(events)});
        } catch (error) {
            console.log(error);
        }
    }

    renderRow(row) {
        return (
            <EventCard
                event={row}/>
        );
    }

    renderLoading() {
        return (
            <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                <Spinner />
                <Text>Loading...</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});