import React, {Component} from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    ListView,
    Text,
    View
} from 'react-native';

import {Scene, Router, TabBar, Modal, Actions, Reducer, ActionConst} from 'react-native-router-flux'
import {Spinner} from 'native-base';
import Api from '../Api';
import PlaceCard from '../components/PlaceCard';

export default class PlacesScreen extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            loading: true,
            places: ds.cloneWithRows([]),
            position: null,
        };
        this.renderRow = this.renderRow.bind(this);
    }

    componentDidMount() {
        this.loadPlaces();

        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({position});
            },
            (error) => Alert.alert(JSON.stringify(error)),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    }


    render() {
        if (this.state.loading) {
            return this.renderLoading();
        }

        return (
            <View style={{flex: 1}}>
                <ListView
                    dataSource={this.state.places}
                    renderRow={this.renderRow}
                />
            </View>
        );
    }

    async loadPlaces() {
        try {
            this.setState({loading: true});
            let response = await Api.getPlaces();
            let places = await response.json();
            places = places.academ_places;

            this.setState({loading: false, places: this.state.places.cloneWithRows(places)});
        } catch (error) {
            console.log(error);
        }
    }

    renderRow(row) {
        return (
            <PlaceCard
                position={this.state.position}
                place={row}/>
        );
    }

    renderLoading() {
        return (
            <View style={{flex: 1, justifyContent:'center', alignItems: 'center'}}>
                <Spinner />
                <Text>Loading...</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});