const api = (function () {
    const baseUrl = 'http://token-shop.ru';

    return {
        getNews: getNews,
        getPlaces: getPlaces,
        getWeather: getWeather,
        getClasses: getClasses,
        getDepartments: getDepartments,
        getAcadem: getAcadem,
        getLessons: getLessons,
        getFullNews: getFullNews,
        getDepartmentsClasses: getDepartmentsClasses
    };
    function getNews() {
        return getData('/api/NEWS');
    }

    function getPlaces() {
        return getData('/api/ACADEM/PLACES');
    }

    function getWeather() {
        return getData('/api/WEATHER');
    }

    function getClasses() {
        return getData('/api/CLASSES');
    }

    function getDepartments() {
        return getData('/api/DEPARTMENTS');
    }

    function getAcadem() {
        return getData('/api/ACADEM')
    }

    function getLessons(groupId) {
        let url = '/api/CLASSES/' + groupId.toString() + '/LESSONS';
        return getData(url);
    }

    function getFullNews(url) {
        return getData('/api/NEWS/' + url);
    }

    function getDepartmentsClasses(departmentId) {
        return getData('/api/DEPARTMENTS/' + departmentId + '/CLASSES');
    }

    function getData(url) {
        return fetch(baseUrl + url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: null
        });
    }

})();

export default api;
